# Copyright 2015 Dylan Ashley
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json


class Task():
    """
    Represents a single task.
    """
    def __init__(self, name, date, topic):
        self.name = str(name)
        self.date = str(date)
        self.topic = str(topic)

    def set_name(self, name):
        """
        Sets the name of task to name.
        """
        self.name = name

    def set_date(self, date):
        """
        Sets the date of task to date.
        """
        self.date = date

    def set_topic(self, topic):
        """
        Sets the topic of task to topic.
        """
        self.topic = topic

    def get_name(self):
        """
        Returns the name of task.
        """
        return self.name

    def get_date(self):
        """
        Returns the date of task.
        """
        return self.date

    def get_topic(self):
        """
        Returns the topic of task.
        """
        return self.topic


def from_json(json):
    """
    Returns a new Task object from the JSON representation of a Task.
    """
    return Task(json["name"], json["date"], json["topic"])


class TaskJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Task):
            return {"name": obj.get_name(),
                    "date": obj.get_date(),
                    "topic": obj.get_topic()}
        else:
            return json.JSONEncoder.default(self, obj)
