# Copyright 2015 Dylan Ashley
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from time import sleep
import argparse
import planner
import shutil


class Interface:
    """
    Represents the textual interface for plan.py.
    """
    def __init__(self):
        try:  # fails with AttributeError in python2
            self.width = shutil.get_terminal_size().columns
        except AttributeError:
            self.width = 80
        self.parse_args()
        self.planner = planner.Planner(self.planfile)
        try:
            print("Welcome to plan.py!")
            print("Enter help for a list of commands.")
            print("-" * self.width)
            while True:
                try:  # fails with NameError in python3
                    self.command = raw_input("Enter a command >> ")
                except NameError:
                    self.command = input("Enter a command >> ")
                self.command = self.command.strip().lower().split()
                if self.command != "":
                    if len(self.command[0]) < 3:
                        self.shortcuts()
                    if self.command[0] == "exit" or self.command[0] == "quit":
                        break
                    try:
                        self.handler()
                    except KeyboardInterrupt:
                        pass
                    print("-" * self.width)
        except EOFError:
            self.planner.save()
        else:
            print("Goodbye")
            self.planner.save()
            sleep(1)

    def parse_args(self):
        """
        Function that obtains planfile from the command line.
        """
        parser_description = ()
        parser = argparse.ArgumentParser(description="This is a very simple "
                                                     "terminal application "
                                                     "to manage day-to-day "
                                                     "tasks.")
        parser.add_argument("planfile", help="File to store the planner in.")
        parser.parse_args(namespace=self)

    def handler(self):
        """
        Function that attempts to carry out self.command.
        """
        if self.command[0] == "help":
            self.com_help()
        elif self.command[0] == "show" or self.command[0] == "ls":
            self.com_show()
        elif self.command[0] == "new" and self.command[1] != "":
            while len(self.command) < 4:
                self.command.append("-")
            self.com_new(self.command[1], self.command[2], self.command[3])
        elif self.command[0] == "set":
            if self.command[1] == "date":
                self.com_date(self.command[2], self.command[3])
            elif self.command[1] == "name":
                self.com_name(self.command[2], self.command[3])
            elif self.command[1] == "topic":
                self.com_topic(self.command[2], self.command[3])
            else:
                print("Incorrect Criteria!")
        elif self.command[0] == "delete":
            self.com_delete(self.command[1])
        else:
            print("Incorrect Command!")

    def shortcuts(self):
        """
        Function that handles shortcut commands.
        """
        if self.command[0] == "s":
            self.command[0] = "show"
        elif self.command[0] == "n":
            self.command[0] = "new"
        elif self.command[0] == "sd":
            self.command[0] = "set"
            self.command.insert(1, "date")
        elif self.command[0] == "sn":
            self.command[0] = "set"
            self.command.insert(1, "name")
        elif self.command[0] == "st":
            self.command[0] = "set"
            self.command.insert(1, "topic")
        elif self.command[0] == "d":
            self.command[0] = "delete"
        elif self.command[0] == "q":
            self.command[0] = "quit"
        else:
            pass

    def com_help(self):
        """
        Function that prints out the various commands and their syntax
        for the user.
        """
        print('Commands:')
        print('"show"                          : '
              'prints out the list of all the tasks')
        print('"new [name] [date] [topic]"     : '
              'creates a new task with [name] [topic] [date]')
        print('"set date [name] [date]"        : '
              'sets the date of [name] to be [date]')
        print('"set name [oldName] [newName]"  : '
              'sets the name of [oldName] to be [newName]')
        print('"set topic [name] [topic]"      : '
              'sets the topic of [name] to be [topic]')
        print('"delete [name]"                 : '
              'deletes the first task with [name]')
        print('"quit" or "exit"                : '
              'saves and quits the program')
        print('\n')
        print('Shortcuts:')
        print('"s"  -> "show"')
        print('"n"  -> "new"')
        print('"st" -> "set topic"')
        print('"sd" -> "set date"')
        print('"sn" -> "set name"')
        print('"d"  -> "delete"')
        print('"q"  -> "quit"')

    def com_show(self):
        """
        Function that prints out all of the tasks for the user.
        """
        data = self.planner.get_all()
        for name, topic, date in data:
            print("{} {} {}".format(name, topic, date))

    def com_new(self, name, topic, date):
        """
        Makes a new task with name, topic, and date.
        """
        self.planner.new(name, topic, date)

    def com_date(self, name, date):
        """
        Changes the date of name to date.
        """
        self.planner.change_date(name, date)

    def com_name(self, old_name, new_name):
        """
        Changes the name of old_name to new_name.
        """
        self.planner.change_name(old_name, new_name)

    def com_topic(self, name, topic):
        """
        Changes the topic of name to topic.
        """
        self.planner.change_topic(name, topic)

    def com_delete(self, name):
        """
        Deletes the first task with name.
        """
        self.planner.delete(name)
