# Copyright 2015 Dylan Ashley
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import task


class Planner:
    """
    Represents the database for plan.py.
    """
    def __init__(self, filename):
        self.filename = filename
        try:
            with open(self.filename, "r") as infile:
                self.data = [task.from_json(item)
                             for item in json.load(infile)]
        except IOError:
            self.data = list()

    def new(self, name, date, topic):
        """
        Adds a new task to the database with name, date, and topic.
        """
        self.data.append(task.Task(name.lower(), date.lower(), topic.lower()))

    def get_task(self, name):
        """
        Returns the name, date, and topic of a task with name.
        """
        for task in self.data:
            if task.get_name() == name:
                return task.get_name(), task.get_date(), task.get_topic()

    def get_all(self):
        """
        Returns the name, date, and topic of all the tasks in a list.
        """
        data = list()
        for task in self.data:
            data.append((task.get_name(), task.get_date(), task.get_topic()))
        return data

    def change_name(self, old_name, new_name):
        """
        Changes the name of a task with old_name to new_name.
        """
        old_name = old_name.lower()
        new_name = new_name.lower()
        a, date, topic = self.get_task(old_name)
        self.delete(old_name)
        self.new(new_name, date, topic)

    def change_date(self, name, date):
        """
        Changes the date of a task with name to date.
        """
        name = name.lower()
        date = date.lower()
        a, b, topic = self.get_task(name)
        self.delete(name)
        self.new(name, date, topic)

    def change_topic(self, name, topic):
        """
        Changes the topic of a task with name to topic.
        """
        name = name.lower()
        topic = topic.lower()
        a, date, b = self.get_task(name)
        self.delete(name)
        self.new(name, date, topic)

    def delete(self, name):
        """
        Deletes the first task in the database with name.
        """
        name = name.lower()
        for i in range(len(self.data)):
            if self.data[i].get_name() == name:
                del self.data[i]
                break

    def save(self):
        """
        Saves the database to plan.txt.
        """
        with open(self.filename, "w") as outfile:
            json.dump(sorted(self.data, key=lambda x: x.get_date()),
                      outfile,
                      indent=4,
                      cls=task.TaskJSONEncoder)
