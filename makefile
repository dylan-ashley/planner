# Copyright 2015 Dylan Ashley
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

all: __main__.py interface.py planner.py task.py
	echo '#!/usr/bin/env python' > planner
	zip planner.zip __main__.py interface.py planner.py task.py
	cat planner.zip >> planner
	rm planner.zip
	chmod +x planner

clean: planner
	rm -f planner
